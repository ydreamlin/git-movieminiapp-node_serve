var mysql = require('mysql');

var db = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '123',
  database : 'movieminiapp'
});

db.connect((err) => {
    if (err)  throw err;
    console.log('Connected to database');
});

global.dbexec = function (sql, params) {
    return new this.Promise(function (resolve, reject) {
        db.query(sql, params, (err, result) => {
            if (err) reject({ code: '9999', message: err });
            resolve({ code: '0000', message: '操作成功', data: result});
        });
    });
}