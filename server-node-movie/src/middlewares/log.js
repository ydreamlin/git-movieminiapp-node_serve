module.exports = function () {
    return function (req, res, next) {
        console.log('\n\n')
        console.log('请求接口 url: %o', req.url)
        console.log('请求方式 method: %o', req.method)
        console.log('参数 params: %o', req.params)
        console.log('参数 query: %o', req.query)
        console.log('参数 body: %o', req.body)
        console.log('\n\n')
        
        next()
    }
}