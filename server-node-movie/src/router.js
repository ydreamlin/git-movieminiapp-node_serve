var movie = require('./routers/movie')
var comment = require('./routers/comment')

module.exports = function (app) {
    app.get('/movies', movie.getMovieList);
    app.get('/movie/:id', movie.getMovieById);

    app.get('/comments',comment.getComment);
    app.post('/comment',comment.postComment)
}