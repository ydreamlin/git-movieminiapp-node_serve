const axios = require('axios');

module.exports = {
    getMovieList: async function (req, res) {
        const { start, count } = req.query
        const result = await axios.get(`http://api.douban.com/v2/movie/in_theaters?apikey=0df993c66c0c636e29ecbb5344252a4a&start=${start}&count=${count}`)
        res.json(result.data)
    },

    getMovieById: async function (req, res) {
        const { id } = req.params
        const result = await axios.get(`http://api.douban.com/v2/movie/subject/${id}?apikey=0df993c66c0c636e29ecbb5344252a4a`)
        res.json(result.data)
    },
}