const axios = require('axios');

module.exports = {
    getComment: async function (req, res) {
        const { movieId } = req.query
        const sql = 'select * from comment where movieId = ?'
        const result = await dbexec(sql,[movieId])
        res.json(result)
    },

    postComment: async function (req, res) {
        const { comment, img, score, movieId } = req.body
        const sql = 'insert into comment (comment, img, score, movieId, create_date) values (?, ?, ?, ?, now() )'
        const result = await dbexec(sql, [comment, img, score, movieId])
        res.json(result)
    },
}