const express = require('express');
const bodyParser = require('body-parser');
const log = require('./middlewares/log')
const access = require('./middlewares/access')
require('../utils/db')

const app = express();

// 解析 req.body 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// 打印请求的参数、方式、数据
app.use(log())

// 解决跨越问题
app.use(access());

const router = require('./router');
router(app);

const server = app.listen(3000, function () {
    console.log('Listening on port %d', server.address().port);
});