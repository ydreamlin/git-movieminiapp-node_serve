// pages/movie/movie.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    movieList:[],
  },

  getMovieList: function () {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    wx.request({
      url: 'http://localhost:3000/movies',
      data: {
        start: that.data.movieList.length,
        count: 10
      },
      success(res) {
        that.setData({
          movieList: res.data.subjects
        })
        wx.hideLoading() 
      },
      fail(res) {
      }
    })
  },

  handleChangecomment : function (event) {
    wx.navigateTo({
      url: `../comment/comment?movieid=${event.target.dataset.movieid}`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
 

  onLoad: function (options) {
    this.getMovieList();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getMovieList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})