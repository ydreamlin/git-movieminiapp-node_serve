// pages/comment/comment.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    commentList:[],
    movieid:-1 ,
    detail:{},
    content:'',
    score: 5,
    images:[],//上传照片的图片
    imgs: [] //查询comments里的图片
  },
  getComments: function () {
    var that = this
    wx.request({
      url: 'http://localhost:3000/comments',
      data: {
        movieId: that.data.movieid,
      },
      success(res) {
        console.log(res.data.data)
        res.data.data = res.data.data.map(item => {
           item.img = item.img.split(',');
            return item
            })
        that.setData({
          commentList: res.data.data,
        })
      }
    })
  },

  getComment:function () {
    var that = this
    wx.request({
      url: 'http://localhost:3000/comment',
      data: {
        movieId: that.data.movieid,
        comment: that.data.content,
        score: that.data.score,
        img: that.data.images.join(',')
      },
      method: "POST",
      success(res) {
        console.log(res)
      }
    })
  },

  onScore(event) {
    console.log(event.detail)
    this.setData({
      score: event.detail
    });
  },
  commentContent(event) {
    // event.detail 为当前输入的值
    var that = this
    that.setData({
      content: event.detail
    })
  },
  choosepic:function () {
    var that = this
    wx.chooseImage({
      count: 9,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success(res) {
        // tempFilePath可以作为img标签的src属性显示图片
        const tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths)
        that.setData({
          images: tempFilePaths
        })
      }
    })
  },
  submitComment: function () {
    this.getComments();
    this.getComment();
    this.setData({
      content: '',
      score: 5,
      images: [],
    })
    this.getComments();
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (event) {
    wx.showLoading({
      title: '加载中',
    })
    this.setData({
      movieid: event.movieid
    })
    var that = this
    var id = event.movieid
    console.log(event.movieid)
    wx.request({
      url: `http://localhost:3000/movie/${id}`,
      success(res) {
        console.log(res.data)
        that.setData({
          detail: res.data
        })
        that.getComments();
        wx.hideLoading()
      },
      fail(res) {
        console.error(err)
      }
    })
    
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})